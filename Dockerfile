FROM node:12-alpine

RUN npm install -g json-server

ENTRYPOINT ["/usr/local/bin/json-server"]
