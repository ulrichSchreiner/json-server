JSON-SERVER
###########

Docker image of [json-server](https://github.com/typicode/json-server). If you start this image, please
use `docker run --init` so docker starts an init process. This is needed, because node ignores `SIGINT`
otherwise you cannot break the running server.
