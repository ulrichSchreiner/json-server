.PHONY: build
build:
	docker build -t ulrichschreiner/json-server .

.PHONY: push
push:
	docker push ulrichschreiner/json-server
